import React, { useEffect, useState, useRef } from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Header from '../components/header';
import Listlog from '../components/listlog'
import { Paper } from '@material-ui/core';
import io from 'socket.io-client';
import ss from 'socket.io-stream';
import handleAuthSSR from '../utils/auth';
// import { FixedSizeList } from 'react-window';
// import Inputbox from '../components/inputbox'

interface Props {
  stars?: any;
  disconnect?: boolean;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      overflow: 'auto',
      margin: `${theme.spacing(2)}px auto`,
      padding: theme.spacing(1),
      maxWidth: 500,
    },
    paperMcFirst: {
      backgroundColor: theme.palette.background.paper,
      maxHeight: 600, 

    },
    listroot: {
      maxHeight: 550,
      overflowX: 'hidden',
    },
    paperMcSecond: {
      maxHeight: 90,
      margin: `${theme.spacing(2)}px auto`,
      padding: theme.spacing(1),
    },
    listSection: {
      backgroundColor: 'inherit',
    },
    ul: {
      backgroundColor: 'inherit',
      padding: 0,
    },
  }),
);

const Mc = () => {
    let stats: boolean = false; 
    const [logs, setLogs] = useState([]);
    const scrollRef = useRef(null);
    const socket = io('http://34.84.136.71:9009/logs');
    
    useEffect(() => {
      socket.on("connect",function() {
        ss(socket).on('file', function(stream, data) {
          setLogs([]);
          let binaryString: any = "";
          stream.on('data', (data) => {
            const socketDatas = [];
            for(let i = 0; i < data.length; i++) {
              binaryString += String.fromCharCode(data[i]);
            }
            binaryString.trim().split(/\n+/).forEach(res => {
              // socketDatas.push(res.length >= 95 ? res.substring(0, 95) + `...`: res);
              socketDatas.push(res);
            });
            setLogs(logs => [...logs, ...socketDatas]);
          });
          stream.on('end', (data) => {
            binaryString = "";
          });
        });
      });
    },[]);


    useEffect(() => {
      if (scrollRef.current) {
        scrollRef.current.scrollIntoView({ behaviour: "smooth" });
      }
    }, [logs]);

    console.log(logs);
    const classes = useStyles();
  
    return (
      <React.Fragment>
        <CssBaseline />
        <Header title={'admin'}/>
        <div className={classes.root}>
          <Paper className={classes.paperMcFirst}>
            <Listlog logs={logs}/>
          </Paper>
        </div>
      </React.Fragment>
      // <div className={classes.root}>
      //   <Paper className={classes.paperMC1}>
      //     <List dense={true} className={classes.list}>
      //       { 
      //         logs.map((log: string, i: number) => 
      //         <ListItem key={i}>
      //           <ListItemText 
      //             primary={log}
      //           />
      //         </ListItem>)
      //       }
      //         <span ref={scrollRef} />
      //       </List>
      //   </Paper>
      //   {/* <Paper className={classes.paperMC2}>
      //     <Inputbox />
      //   </Paper> */}
      // </div>
    );
}


Mc.getInitialProps = async (ctx) => {
  // Must validate JWT
  // If the JWT is invalid it must redirect
  // back to the main page. You can do that
  // with Router from 'next/router
  await handleAuthSSR(ctx);
  // Must return an object
  return {}
}
// export async function getServerSideProps() {
//   console.log('ctx test');
//   const socket = await io('http://34.84.136.71:9009/logs'); // .env
//   console.log(socket);
//   return { props: {
//       'socket': 'socket'
//     }
//   }
// }




export default Mc;