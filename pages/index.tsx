import React, { useState, useEffect } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Header from '../components/header';
import Signin from '../components/signin';
import Signup from '../components/signup';
import { useRouter } from 'next/router'

import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      overflow: 'auto',
      padding: theme.spacing(0, 1),
    }
  }),
);

const Index = () => {
  const router = useRouter()
  const classes = useStyles();
  const [disconnect, setDisconnect] = useState(false);
  const [account, setAccount] = useState([{'ac':'','pw':''}]);
  const [loginStatus, setLoginStatus] = useState(false);
  // router.push('/about')
  useEffect(() => {
    console.log('useEffect');
    if(loginStatus || localStorage.getItem('token')) {
      router.push('/mc')
    }
  },[loginStatus])

  return (
    <React.Fragment>
      <CssBaseline />
      <Header title={'mc'}/>
        <Container maxWidth="lg">
          <div className={classes.root}>
            {account ? 
              <Signin setLoginStatus={setLoginStatus} setAccount={setAccount} account={account} /> :
              <Signup setAccount={setAccount} />
            }
            
          </div>
        </Container>
    </React.Fragment>
  );
}

export default Index;